<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use Firebase\JWT\JWT ;  

$router->get('/list_orders',function() {
    $results = app('db')->select("SELECT * FROM orders");
    return response()->json($results);
});

$router->post('/add_orders',function(Illuminate\Http\Request $request) {
    $shipmentID = $request->input("shipmentID");
    $orderID = $request->input("orderID");
    $orderName = $request->input("orderName");

    $orderDate = strtotime($request->input("orderDate"));
    $orderDate = date('Y-m-d H:i:s',$orderDate);

    $query = app('db')->insert('INSERT into orders
                                       (shipmentID, orderID , orderName , orderDate) VALUES (?,?,?,?)',
                                       [$shipmentID,
                                        $orderID,
                                        $orderName,
                                        $orderDate] );
   return "OK" ;

});

$router->put('/update_order',function(Illuminate\Http\Request $request) {
    $shipmentID = $request->input("shipmentID");
    $orderID = $request->input("orderID");
    $orderName = $request->input("orderName");

    $orderDate = strtotime($request->input("orderDate"));
    $orderDate = date('Y-m-d H:i:s',$orderDate);

    $query = app('db')->update('UPDATE orders
                                        SET
                                            orderID=?,
                                            orderName =?,
                                            orderDate =?

                                        WHERE
                                          shipmentID =?',
                                        [ 
                                          $orderID,
                                          $orderName,
                                          $orderDate,
                                          $shipmentID] );
                                       
   return "OK" ;

});


$router->delete('/delete_orders',function(Illuminate\Http\Request $request) {
    $shipmentID = $request->input("shipmentID");

    $query = app('db')->delete('DELETE FROM orders
                                    WHERE 
                                        shipmentID=?',
                                       [$shipmentID] );
    return "OK" ;
});   

$router->post('/register_product',function(Illuminate\Http\Request $request){
    $userID = $request->input("userID");
    $name = $request->input("name");
    $username = $request->input("username");
    $surname = $request->input("surname");
    $password = app('hash')->make($request->input("password"));

    $query = app('db')->insert('INSERT into register
                                        (userID, name, username ,surname , password ) 
                                        VALUES (?,?,?,?,?)',
                                        [ $userID,
                                          $name,
                                          $username,
                                          $surname,
                                          $password ] );
    return "OK" ;
});

$router->post('/login_product', function(\Illuminate\Http\Request $request) {
	$username = $request->input("username");
	$password = $request->input("password");

	$result = app('db')->select('SELECT userID, password FROM user WHERE username=?',
									[$username]);
	$loginResult = new stdClass();
	
	if(count ($result) == 0) {
			$loginResult->status = "FAIL";
			$loginResult->reason = "User is not founded";
	}else {
		if(app('hash')->check($password, $result[0]->password)){
            $loginResult->status = "success";

			$payload = [
				'iss' => "product_system",
				'sub' => $result[0]->userID,
				'iat' => time(),
				'exp' => time()+ 30 * 60 * 60,
            ];

			$loginResult->token = JWT::encode($payload,env('APP_KEY'));

            return response()->json($loginResult);
			
		}else {
			$loginResult->status = "FAIL";
			$loginResult->reason = "Incorrect Password";
            return response()->json($loginResult);
		}
	}
	return response()->json($loginResult);
});

